package main

import (
	"fmt"
	"image"
	"image/png"
	"os"
	"time"

	"github.com/kbinani/screenshot"
)

type pixel struct {
	r, g, b, a uint32
}

func main() {
	loadImage("images/trash.png")
	allScreens := getScreen(false)
	for i := 0; i < len(allScreens); i++ {
		fmt.Println("From all screens: ", allScreens[i][0][7])
	}
}

func getScreen(saveImage bool) [][][]pixel {
	activeDisplays := screenshot.NumActiveDisplays()
	screenshotArray := make([][][]pixel, activeDisplays)
	for i := 0; i < activeDisplays; i++ {
		displayBounds := screenshot.GetDisplayBounds(i)
		img, err := screenshot.CaptureRect(displayBounds)
		if err != nil {
			panic(err)
		}
		fmt.Println("New func:")
		newStart := time.Now()
		pixelArray := toPixelArray(img)
		screenshotArray[i] = pixelArray
		timeTrack(newStart, "New function")
		fmt.Println(pixelArray[0][0])
		fmt.Println(pixelArray[0][7])
		if saveImage {
			filename := fmt.Sprintf("%d_%dx%d.png", i, displayBounds.Dx(), displayBounds.Dy())
			file, err := os.Create(filename)
			if err != nil {
				panic(err)
			}
			defer file.Close()
			png.Encode(file, img)

		}
	}
	return screenshotArray
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("%s took %s\n", name, elapsed)
}

func toPixelArray(img image.Image) [][]pixel {
	bounds := img.Bounds()
	fmt.Println(bounds.Dx(), " x ", bounds.Dy())
	pixels := make([][]pixel, bounds.Dx())
	for x := 0; x < bounds.Dx(); x++ {
		pixels[x] = make([]pixel, bounds.Dy())
		for y := 0; y < bounds.Dy(); y++ {
			r, g, b, a := img.At(x, y).RGBA()
			pixels[x][y].r = r
			pixels[x][y].g = g
			pixels[x][y].b = b
			pixels[x][y].a = a
		}
	}
	// fmt.Println("Run complete!")
	return pixels
}

func loadImage(filename string) [][]pixel {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	img, err := png.Decode(f)
	if err != nil {
		panic(err)
	}

	return toPixelArray(img)
}
